# Pautas y técnicas de programación en C

"Pautas y técnicas de programación en C" es un libro introductorio 
a técnicas intermedias
y avanzadas de programación en C, escrito por [Pedro Fortuny 
Ayuso](http://pfortuny.net).

## Contenido

Tomando como punto de partida el [problema del circuito
hamiltoniano](http://en.wikipedia.org/wiki/Hamiltonian_path_problem), se
tratan las materias siguientes:

  - El uso de makefiles.
  - Llamadas al sistema y gestión de errores.
  - Estructuras, estructuras anidadas
  - Números aleatorios
  - Eliminación de la recursión
  - Reutilización de código
  - Optimización mediante cambio de algoritmos
  - Programación en paralelo con hilos POSIX
  - Introducción a comunicación entre procesos (IPC)

## Código implementado

A lo largo del libro, se implementan diversas aplicaciones:

  - Un generador de grafos con un circuito hamiltoniano.
  - Un buscador de circuitos hamiltonianos en grafos. Este
    programa se reproduce de cuatro maneras diferentes, cada
    una más rápida que la anterior.
  - Una aplicación que verifica si un camino es un circuito
    hamiltoniano de un grafo o no.

Todo el código se ha probado en Linux y en OS X.

## Qué se incluye

En este repositorio púbilo se incluye:

  - El libro en formato pdf listo para ser impreso y encuadernado.
    (Se ha realizado en un formato de página más pequeño que A4 para
    que sea más agradable su lectura).
  - Todo el código de las secciones finales de cada capítulo relevante.

## Compilación

El archivo `makefile.true` continene las directrices necesarias
para generar todos los programas del libro en todas sus versiones.
En concreto:

  1. `make-ham`: el generador de grafos.
  2. `seek-ham-rec`: el buscador de circuitos hamiltonianos. Versión
     recursiva.
  3. `seek-ham-unrec`: el mismo buscador, versión no recursiva.
  4. `seek-ham-refactor`: el buscador, tras una refactorización de código.
  5. `seek-ham-adj`: el buscador, versión utilizando matrices de adyacencia.
  6. `seek-ham-thread`: el buscador en paralelo.
  7. `test-ham`: el verificador (si un camino en un grafo es hamiltoniano
       o no).

Así pues, para generar, por ejemplo, la versión con la matriz de adyacencia
del programa de búsqueda, puede usarse `make` como:

    $ make -f makefile.true seek-ham-adj

y se generará el programa `seek-ham-adj`.

La receta `clean` limpia todos los ejecutables y códigos objeto creados.

## Requisitos

Se requiere la librería [OpenSSL](http://www.openssl.org) para compilar
el programa
de generación de grafos. Para el resto, técnicamente no se requiere pero para
simplificar, se ha incluido en la macro `LIBS` de `makefile.true` en todas
las compilaciones, así que, si se usa `make` para construirlos, será necesario
que OpenSSL esté instalada.

## Licencia y distribución

El libro es propiedad de Pedro Fortuny Ayuso, quien posee el copyright. El
código de este repositorio está sujeto a una licencia ISC, como se puede leer
en el archivo `licence`.

La redistribución del libro no está permitida sin permiso expreso del autor.
Ahora bien: está disponible gratuitamente en este sitio web, así que quizás
no sea precisa.

## Precio

El libro, tal y como se presenta en este repositorio, 
es (obviamente) accesible de manera directa. Ahora bien, el autor asume que
cualquier lector interesado puede contribuir con al menos 1Euro, mediante
PayPal, ingresables en la cuenta `info@pfortuny.net`, a nombre de
Pedro Fortuny Ayuso.

Es decir: el libro tiene un precio pero el autor no tiene medios para
garantizar el cobro.

## ¿Pero por qué no es Creative Commons?

No se ha publicado este material bajo una licencia Creative Commons por
dos razones.

  1. Las licencias CC son irrevocables y no sé si en el futuro necesitaré
     este material para ganarme la vida.
  2. El hecho de que tenga copyright no implica que "el autor prohiba su
     uso si no se puede pagar por él" como es evidente. Para esto no hace
     falta una licencia de ese estilo.

## Errores

Es evidente que el libro contendrá errores, erratas, inconsistencias y otras
molestas inconveniencias. Agradecería que se me comunicaran con un mensaje
de correo electrónico a `info@pfortuny.net`.


