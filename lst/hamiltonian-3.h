/* hamiltonian.h (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */

#include <sys/types.h>

#define MAXINT 0xFFFFFFFF

#define ERR_PARAMS 1
#define ERR_USG    2

#define USAGE_MAKE_HAM "\
(C) 2013 --- Pedro Fortuny Ayuso\n\
Usage:\n\
\n\
$ %s v e\n\
\n\
where\n\
v: number of vertices\n\
e: number of edges\n\
and v < n.\n\
"
#define ERR_OS 255
#define ERR_OS_S \
        "There was a system error.\n"

#define NOEDGES "\
There cannot be any edges in a graph with less than 2 vertices.\n"

typedef struct edge_s {
        u_int a;
        u_int b;
} edge_t;

typedef struct graph_s {
	u_int   vert_n;
        u_int   edge_n;
	u_int  *vertex;
	edge_t *edge;
} graph_t;

u_int new_graph(graph_t *,u_int , u_int );

u_int rand_int(u_int );
