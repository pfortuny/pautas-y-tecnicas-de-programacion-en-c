/* hamiltonian.h (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */
#include <sys/types.h>

#define ERR_PARAMS 1
#define ERR_USG 1

#define USAGE_MAKE_HAM "\
(C) 2013 --- Pedro Fortuny Ayuso\n\
Usage:\n\
\n\
$ %s v e\n\
\n\
where\n\
v: number of vertices\n\
e: number of edges\n\
and v < n.\n\
"

#define ERR_OS 255
#define ERR_OS_S \
        "There was a system error.\n"

#define NOEDGES "\
There cannot be any edges in a graph with less than 2 vertices.\n"

#define ERR_BAD_STRING 6
#define ERR_BAD_STRING_S \
        "The input string is not a well-formed graph.\n"

#define FAIL_NO_HAM   65
#define FAIL_NO_HAM_S \
        "There is no hamiltonian circuit in the graph.\n"

/* this must be defined somewhere... */
#define MAXINT 0xFFFFFFFF

typedef struct edge_s {
        u_int a;
        u_int b;
} edge_t;

typedef struct graph_s {
	u_int   vert_n;
        u_int   edge_n;
	u_int  *vertex;
	edge_t *edge;
} graph_t;


typedef struct adjacent {
        u_int vertex;
        u_int edge_n;
        struct adjacent *next;
} adjacent_t;


int new_graph(graph_t *, u_int , u_int );

u_int rand_int(u_int );

int process_graph(char *);

u_int seek_and_join(graph_t, 
                    u_int *,
                    u_int *,
                    u_int *,
                    u_int *,
                    u_int *);

