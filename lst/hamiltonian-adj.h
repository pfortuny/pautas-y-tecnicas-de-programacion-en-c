/* hamiltonian.h (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */
#include <sys/types.h>

#define ERR_PARAMS 1
#define ERR_USG 1

#define USAGE_MAKE_HAM "\
(C) 2013 --- Pedro Fortuny Ayuso\n\
Usage:\n\
\n\
$ %s v e\n\
\n\
where\n\
v: number of vertices\n\
e: number of edges\n\
and v < n.\n\
"

#define ERR_OS 255
#define ERR_OS_S \
        "There was a system error.\n"

#define NOEDGES "\
There cannot be any edges in a graph with less than 2 vertices.\n"

#define ERR_BAD_STRING  6
#define ERR_BAD_STRING_S \
        "The input string is not a well-formed graph.\n"

#define ERR_ADJACENCY_0 64
#define ERR_ADJACENCY_0_S \
        "There is a vertex without adjacent ones.\n"

#define FAIL_NO_HAM   65
#define FAIL_NO_HAM_S \
        "There is no hamiltonian circuit in the graph.\n"

#define FAIL_NOT_HAM   66
#define FAIL_NOT_HAM_S \
        "The path is not a hamiltonian circuit.\n"

/* test-ham macros */
#define FAIL_WRONG_EDGE_S \
        "There is a non-existent edge in the sequence.\n"
#define FAIL_WRONG_EDGE 129

#define FAIL_NO_PATH_S \
        "The sequence of edges is not a path.\n"
#define FAIL_NO_PATH 130

#define FAIL_REPEATED_VTX_S \
        "The path goes twice through the same vertex.\n"
#define FAIL_REPEATED_VTX 131

#define FAIL_TOO_MANY_EDGES_S \
        "The path is too long.\n"
#define FAIL_TOO_MANY_EDGES 132

#define FAIL_TOO_SHORT_S \
        "The path is too short.\n"
#define FAIL_TOO_SHORT 133

/* this must be defined somewhere... */
#define MAXINT 0xFFFFFFFF

typedef struct edge_s {
        u_int a;
        u_int b;
} edge_t;

typedef struct graph_s {
	u_int   vert_n;
        u_int   edge_n;
	u_int  *vertex;
	edge_t *edge;
} graph_t;

typedef struct adjacent {
        u_int vertex;
        u_int edge_n;
        struct adjacent *next;
} adjacent_t;

int new_graph(graph_t *, u_int , u_int );

u_int rand_int(u_int );

int process_graph(char *);

int parse_graph(graph_t *, char *);

u_int seek_and_join(graph_t , 
                    u_int *,
                    u_int *,
                    u_int *,
                    u_int *,
                    u_int *);

int push_adjacent(adjacent_t **,
                  u_int ,
                  u_int ,
                  u_int );

int iterate_vertices(adjacent_t **,
                     u_int,
                     u_int *,
                     u_int *,
                     u_int *);
