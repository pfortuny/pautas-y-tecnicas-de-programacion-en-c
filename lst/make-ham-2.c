/* make-ham.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "hamiltonian.h"


int main(int argc, char *argv[]){
        int retval = 0;

        u_int v, e, i;
        graph_t Gr;
        Gr.vertex = NULL;
        Gr.edge   = NULL;

	if(argc != 3){
		fprintf(stderr, USAGE_MAKE_HAM, argv[0]);
                retval = ERR_PARAMS;
                goto FREE;
        }

        v = (u_int) strtol(argv[1], NULL, 10);
        e = (u_int) strtol(argv[2], NULL, 10);

        if(v<=1 && e>0){
                fprintf(stderr, NOEDGES);
                retval = ERR_USG;
                goto FREE;
        }

        retval = new_graph(&Gr, v, e);
        if(retval != 0)
                goto FREE;

        for(i=0; i<v; i++){
                Gr.vertex[i] = i;
        }
        for(i=0; i<e; i++){
                Gr.edge[i].a = i % v;
                Gr.edge[i].b = (i+1) % v;
        }

        for(i=0; i<v; i++){
                printf("Vertex [%u]: {%u}\n", i, Gr.vertex[i]);
        }
        for(i=0; i<e; i++){
                printf("Edge [%u]: {%u, %u}\n",
                       i, 
                       Gr.edge[i].a,
                       Gr.edge[i].b);
        }

FREE:
        free(Gr.vertex);
        free(Gr.edge);
        exit(0);
}
