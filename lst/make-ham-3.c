/* make_ham.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <openssl/rand.h>
#include <err.h>

#include "hamiltonian.h"

int main (int argc, char *argv[]){
        int retval;

        u_int v, e, i;
        graph_t Gr;

        u_int last;

	if(argc != 3){
		fprintf(stderr, USAGE_MAKE_HAM, argv[0]);
                exit(ERR_PARAMS);
        }

        v = (u_int) strtol(argv[1], NULL, 10);
        e = (u_int) strtol(argv[2], NULL, 10); 

        if(v<=1 && e>0){
                fprintf(stderr, NOEDGES);
                exit(ERR_USG);
        }

        retval = new_graph(&Gr, v, e);
        if(retval != 0)
                goto FREE;

        /* 
         * Name the vertices.
         */
	for(i=0; i<Gr.vert_n; i++){
		Gr.vertex[i]=i;
	}

        /* 
         * Fisher-Yates on the vertices.
         */
	for(i=0; i < Gr.vert_n-1; i++){
                u_int tmp;
                u_int new_pos;

                new_pos = i + rand_int(Gr.vert_n-i);

		tmp = Gr.vertex[i];
		Gr.vertex[i] = Gr.vertex[new_pos];
		Gr.vertex[new_pos] = tmp;
	}

        /* 
         * Build the elementary hamiltonian circuit.
         */
	for(i=0; i < Gr.vert_n; i++){
                Gr.edge[i].a = Gr.vertex[i];
                Gr.edge[i].b = Gr.vertex[(i + 1) % (Gr.vert_n)];
	}

        /* 
         * Generate the remaining edges in a random way.
         */
	for(i=Gr.vert_n; i < Gr.edge_n; ){
		u_int k,l;
                k = rand_int(Gr.vert_n);
                l = rand_int(Gr.vert_n);

                /* no ribbons [a,a] */
		if(k==l)
			continue;

                Gr.edge[i].a = Gr.vertex[k];
                Gr.edge[i].b = Gr.vertex[l];
                i++;
	}

        /* 
         * Fisher-Yates on the edges.
         */
	for(i=0; i<Gr.edge_n-1; i++){
                edge_t tmp;
                u_int new_pos;
                
                new_pos = i + rand_int(Gr.edge_n-i);

                tmp = Gr.edge[i];
                Gr.edge[i] = Gr.edge[new_pos];
                Gr.edge[new_pos] = tmp;
	}

        /* output all the edges */
	for(i=0; i < Gr.edge_n-1; i++){
		printf("%u,%u;",
                       Gr.edge[i].a,
                       Gr.edge[i].b);
	}
        /* and the last one without trailing semicolon */
        last = Gr.edge_n - 1;
        printf("%u,%u", Gr.edge[last].a, Gr.edge[last].b);
	printf("\n");

FREE:
        free(Gr.edge);
        free(Gr.vertex);
        exit(0);

}

u_int rand_int(u_int r){
        u_int buf;
        u_int max_val;

        max_val = MAXINT - (MAXINT % r);

        while(!RAND_bytes((unsigned char *)&buf, 
                          sizeof(u_int)));
        do {
                while(!RAND_bytes((unsigned char *)&buf, 
                                  sizeof(u_int)));
        } while(buf >= max_val);

        return(buf % r);
}
