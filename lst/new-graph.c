/* new-graph.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "hamiltonian.h"

int new_graph(graph_t *graph, u_int v, u_int e){
        int retval = 0;

        graph->vertex = NULL;
        graph->edge   = NULL;

        graph->vert_n = v;
        graph->edge_n = e;

        graph->vertex = (u_int *)  calloc(v, sizeof(u_int));
        graph->edge   = (edge_t *) calloc(e, sizeof(edge_t));
        if(graph->vertex == NULL || 
           graph->edge   == NULL){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto RET;
        }
        
RET:
        return retval;
}  
