/* parse-graph.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hamiltonian.h"

/* parse_graph
 *
 * Input:
 * graph_t *graph:  a pointer to a graph structure.
 * char *input_txt: a string containing the graph as per
 *                  make_graph.
 *
 * Output:
 * 0   if the string parses as a graph and the graph has been
 *        properly built in *graph.
 * !=0 if some error happened.
 */
int parse_graph(graph_t *graph, char *input_txt){
        
        int retval = 0;
        /*
         * Parse 'input_txt' (which is morally a list of edges)
         * 'input_txt' is a string of pairs of numbers separated by
         * semicolons, that is, like "1,2;7,8;9,0"
         */

        /* 
         * we need to parse 'input_txt' twice:
         * This first time to get the number of vertices & edges.
         * vertex_nbr holds the 'maximum vertex' which is the number of
         *     vertices-1
         */
        u_int edge_nbr   = 0;
        u_int vertex_nbr = 0;
        u_int i, v1, v2;

        char *parser;
        u_int e = 0;

        if(input_txt == NULL){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto END;
        }

        for(i=0; i<strlen(input_txt); i++){
                
                /* 
                 * Verify that the string is well-formed.
                 */
                if((input_txt[i] < '0'||
                    input_txt[i] > '9') &&
                   input_txt[i] != ','  &&
                   input_txt[i] != ';'){
                        fprintf(stderr, ERR_BAD_STRING_S);
                        retval = ERR_BAD_STRING;
                        goto END;
                }

                if(i == 0 || input_txt[i] == ';'){
                        
                        edge_nbr++;
                        
                        if(sscanf(&input_txt[i + (i!=0)], 
                                  "%u,%u", 
                                  &v1, 
                                  &v2) < 2) {
                                fprintf(stderr, ERR_BAD_STRING_S);
                                retval = ERR_BAD_STRING;
                                goto END;
                        }
                        
                        if(v1>vertex_nbr)
                                vertex_nbr=v1;
                        if(v2>vertex_nbr)
                                vertex_nbr=v2;
                }
        }
        
        /* 
         * Careful: the number of vertices is
         * maximum_vertex + 1
         */
        vertex_nbr++;

        retval = new_graph(graph, vertex_nbr, edge_nbr);
        if(retval != 0)
                goto END;

        /* Second parsing of 'input_txt',
         * now we can assign to each edge its two vertices
         * (the graph has already been built).
         */
        while((parser = strsep(&input_txt, ";"))!=NULL){
                sscanf(parser, "%u,%u", &v1, &v2);
                graph->edge[e].a = v1;
                graph->edge[e].b = v2;
                e++;
        }

END:
        return(retval);
}



/* 
 * push_adjacent: add an element to the adjacency matrix.
 * 
 * Input:
 *
 * adjacent_t **adjacency_m: adjacency_m, which is a vector
 *              of lists (adjacent_t*).
 * v1, v2: vertices. v2 is adjacent to v1.
 * edge_n: order number of the edge [v1,v2] in the list of edges.
 *
 * Ouput: the number of additions to the adjacency matrix
 * performed (0 or 1). If the edge [v1,v2] or [v2,v1] has already
 * appeared, there will be no additions. Thus a return value of
 * 0 is not an error. If some error occurred, -1 is returned.
 *
 * Side effects: 
 * adjacency_m (actually the list adjacency_m[v1]) gets
 * a new element if v2 is adjacent to v1 and it has not been listed
 * yet.
 */
 
int push_adjacent(adjacent_t **adjacency_m,
                  u_int v1,
                  u_int v2,
                  u_int edge_n){
        int retval = 0;

        adjacent_t *adj_v1;
        adj_v1 = adjacency_m[v1];

        /* 
         * All these lines are just for getting to the
         * last adjacent to v1 (if needed), create a new
         * entry and point adj_v1 to this new entry.
         * This is standard stuff in linked lists.
         * In English:
         * If there are no entries for v1 in the adj. matrix,
         *          create the first one and set adj_v1 to this.
         * Else
         *    run through the list of adjacents until the end
         *        (if v2 is in the list, return immediately),
         *    create a new entry and set adj_v1 to this.
         */
        if(adj_v1 == NULL){
                adj_v1 = adjacency_m[v1] = 
                        (adjacent_t *) calloc(1, sizeof(adjacent_t));
                if(adj_v1 == NULL){
                        fprintf(stderr, ERR_OS_S);
                        retval = ERR_OS;
                        goto RET;
                }
                adj_v1->next = NULL;
        }
        else {
                if(adj_v1->vertex == v2)
                        return 0;
                while(adj_v1->next != NULL){
                        adj_v1 = adj_v1->next;
                        if(adj_v1->vertex == v2)
                                return 0;
                }
                adj_v1->next = 
                        (adjacent_t *)calloc(1, sizeof(adjacent_t));
                if(adj_v1->next == NULL){
                        fprintf(stderr, ERR_OS_S);
                        retval = ERR_OS;
                        goto RET;
                }

                adj_v1 = adj_v1->next;
        }

        adj_v1->vertex = v2;
        adj_v1->edge_n = edge_n;

RET:
        return retval;
}
