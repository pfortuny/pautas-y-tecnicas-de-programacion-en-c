/* parse-graph.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hamiltonian.h"

/* parse_graph
 *
 * Input:
 * char *input_txt: a string containing the graph as per
 *                  make_graph.
 * graph_t *graph:  a pointer to a graph structure.
 *
 * Output:
 * 0   if the string parses as a graph and the graph has been
 *        properly built in *graph.
 * !=0 if some error happened.
 */
int parse_graph(graph_t *graph, char *input_txt){
        
        int retval = 0;
        /*
         * Parse 'input_txt' (which is morally a list of edges)
         * 'input_txt' is a string of pairs of numbers separated by
         * semicolons, that is, like "1,2;7,8;9,0"
         */

        /* 
         * we need to parse 'input_txt' twice:
         * This first time to get the number of vertices & edges.
         * vertex_nbr holds the 'maximum vertex' which is the number of
         *     vertices-1
         */
        u_int edge_nbr   = 0;
        u_int vertex_nbr = 0;
        u_int i, v1, v2;

        char *parser;
        u_int e = 0;

        if(input_txt == NULL){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto END;
        }

        for(i=0; i<strlen(input_txt); i++){
                
                /* 
                 * Verify that the string is well-formed.
                 */
                if((input_txt[i] < '0'||
                    input_txt[i] > '9') &&
                   input_txt[i] != ','  &&
                   input_txt[i] != ';'){
                        fprintf(stderr, ERR_BAD_STRING_S);
                        retval = ERR_BAD_STRING;
                        goto END;
                }

                if(i == 0 || input_txt[i] == ';'){
                        
                        edge_nbr++;
                        
                        if(sscanf(&input_txt[i + (i!=0)], 
                                  "%u,%u", 
                                  &v1, 
                                  &v2) < 2) {
                                fprintf(stderr, ERR_BAD_STRING_S);
                                retval = ERR_BAD_STRING;
                                goto END;
                        }
                        
                        if(v1>vertex_nbr)
                                vertex_nbr=v1;
                        if(v2>vertex_nbr)
                                vertex_nbr=v2;
                }
        }
        
        /* 
         * Careful: the number of vertices is
         * maximum_vertex + 1
         */
        vertex_nbr++;
  
        retval = new_graph(graph, vertex_nbr, edge_nbr);
        if(retval != 0)
                goto END;

        /* Second parsing of 'input_txt',
         * now we can assign to each edge its two vertices
         * (the graph has already been built).
         */
        while((parser = strsep(&input_txt, ";"))!=NULL){
                sscanf(parser, "%u,%u", &v1, &v2);
                graph->edge[e].a = v1;
                graph->edge[e].b = v2;
                e++;
        }

END:
        return(retval);
}
