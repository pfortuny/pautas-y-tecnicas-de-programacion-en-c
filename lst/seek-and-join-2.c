/* seek_and_join.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */

u_int seek_and_join(graph_t graph, 
                    u_int *path,
                    u_int *start,
                    u_int *end,
                    u_int *length,
                    u_int *used_vertices){
        u_int i;
        for(i=0; i < graph.edge_n; i++){
                u_int A = graph.edge[i].a;
                u_int B = graph.edge[i].b;

                /*
                 * Last case: length == n-1 is special
                 */
                if(*length == graph.vert_n-1){
                        if((A == *start && B == *end  ) ||
                           (A == *end   && B == *start)){
                                path[(*length)++] = i;
                                return(*length);
                        }
                        continue;
                }

               /* 
                * Can this arc be joined at any of the *endpoints?
                */
                if(A == *start && !used_vertices[B]){
                        *start = B;
                } else if(A == *end && !used_vertices[B]){
                        *end = B;
                } else if(B == *start && !used_vertices[A]){
                        *start = A;
                } else if(B == *start && !used_vertices[A]){
                        *end = A;
                } else 
                        continue;

                used_vertices[A]  = 1;
                used_vertices[B]  = 1;
                path[(*length)++] = i;

                if(seek_and_join(graph, 
                                 path,
                                 start,
                                 end,
                                 length,
                                 used_vertices) == graph.vert_n)
                        return(graph.vert_n);

                /* 
                 * The recursion has not given a solution: 
                 * remove last added edge from path and continue.
                 */
                (*length)--;

                if(*start == A){
                        *start = B;
                        used_vertices[A] = 0;
                } else if(*end == A) {
                        *end = B;
                        used_vertices[A] = 0;
                } else if(*start == B) {
                        *start = A;
                        used_vertices[B] = 0;
                } else {
                        *end = A;
                        used_vertices[B] = 0;
                }
        }

        /* 
         * No edge has been useful for building a solution:
         * return the present length: at this point
         * it is always != graph.vert_n.
         */
        return (*length);
}
