/* seek_ham.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <err.h>
#include <unistd.h>

#include "hamiltonian.h"

/* 
 * seek_ham
 * Given a graph (see make_graph.c) which supposedly contains
 * a Hamiltonian Path, search for one and report it.
 *
 * Input:
 * s: a string describing a graph, as per make_graph.c
 * t: (optional) maximum number of seconds to perform seek
 *
 * Output:
 * 
 * a string of comma-separated numbers which correspond to the
 * edges in the graph building up a hamiltonian path.
 *
 * On stderr, both the received graph and the elapsed time
 * are reported.
 */


int main(int argc, char *argv[]){
        int retval = 0;

        /* verify that input_txt is not NULL */
        if(argc < 2 || argv[1] == NULL){
                fprintf(stderr, ERR_BAD_STRING_S);
                exit(ERR_BAD_STRING);
        }
        
        if((retval = process_graph(argv[1])) != 0){
                exit(retval);
        };

        exit(retval);
}


/* process_graph
 *
 * Input:
 * char *input_txt: a string containing the graph as per
 *                  make_graph
 * Output:
 * int: error number or 0 if success.
 *
 * On stdout, the list or arcs making a hamiltonian path.
 */
int process_graph(char *input_txt){
        int retval = 0;
        
        /* counters for first input parsing */
        u_int edge_nbr   = 0;
        u_int vertex_nbr = 0;
        u_int v1, v2, i;

        graph_t Gr;
        Gr.vertex = NULL;
        Gr.edge   = NULL;

        /* second parsing of input */
        char *parser = NULL;
        u_int e      = 0;

        /* parameters for solution seeking */
        u_int *path          = NULL;
        u_int *used_vertices = NULL;
        u_int path_length    = 0;
        u_int start, end;        

        /*
         * Parse 'input_txt' (which is morally a list of edges)
         * 'input_txt' is a string of pairs of numbers separated
         *             by semicolons,
         *             that is, like "1,2;7,8;9,0"
         */

        /* 
         * we need to parse 'input_txt' twice:
         * This first time to get the number of vertices & edges.
         * vertex_nbr holds the 'maximum vertex' which is
         * the number of vertices minus 1
         */

        if(input_txt == NULL){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto FREE;
        }

        for(i=0; i<strlen(input_txt); i++){
                
                /* 
                 * Verify that the string is well-formed.
                 */
                if((input_txt[i] < '0'||
                    input_txt[i] > '9') &&
                   input_txt[i] != ','  &&
                   input_txt[i] != ';'){
                        fprintf(stderr, ERR_BAD_STRING_S);
                        return(ERR_BAD_STRING);
                }
                
                if(i == 0 || input_txt[i] == ';'){
                        
                        edge_nbr++;
                        
                        if(sscanf(&input_txt[i + (i!=0)], 
                                  "%u,%u", 
                                  &v1, 
                                  &v2) < 2) {
                                fprintf(stderr, ERR_BAD_STRING_S);
                                return(ERR_BAD_STRING);
                        }
                        
                        if(v1>vertex_nbr)
                                vertex_nbr=v1;
                        if(v2>vertex_nbr)
                                vertex_nbr=v2;
                }
        }
        
        /* 
         * Careful: the number of vertices is
         * maximum_vertex + 1
         */
        vertex_nbr++;
  
        retval = new_graph(&Gr, vertex_nbr, edge_nbr);
        if(retval != 0){
                goto FREE;
        }
                
        /* Second parsing of 'input_txt',
         * now we can assign to each edge its two vertices
         * (the graph has already been built).
         */
        while((parser = strsep(&input_txt, ";"))!=NULL){
                sscanf(parser, "%u,%u", &v1, &v2);
                Gr.edge[e].a = v1;
                Gr.edge[e].b = v2;
                e++;
        }

        /*
         * Do seek the hamiltonian path.
         */
        path =          (u_int *)calloc(Gr.vert_n, sizeof(u_int));
        used_vertices = (u_int *)calloc(Gr.vert_n, sizeof(u_int));
        if(path == NULL || used_vertices == NULL){
                fprintf(stderr, "Unable to allocate memory\n");
                retval = ERR_OS;
                goto FREE;
        }

        for(i=0; i<Gr.edge_n; i++){
                u_int A = Gr.edge[i].a;
                u_int B = Gr.edge[i].b;
                start = A;
                end   = B;
                path[path_length++] = i;
                used_vertices[A] = 1;
                used_vertices[B] = 1;
        
                seek_and_join(Gr,
                              path,
                              &start,
                              &end,
                              &path_length,
                              used_vertices);
                if(path_length == Gr.vert_n)
                        break;

                path_length--;
                used_vertices[A] = 0;
                used_vertices[B] = 0;
        }


        /*
         * Failure: report and exit.
         */
        if(path_length != Gr.vert_n){
                fprintf(stderr, FAIL_NO_HAM_S);
                retval = FAIL_NO_HAM;
                goto FREE;
        }

        /* 
         * Success: report on stdout.
         */
        for(i=0; i<vertex_nbr-1; i++)
                printf("%u,", path[i]);
        printf("%u\n", path[i]);

FREE:
        free(path);
        free(used_vertices);
        free(Gr.edge);
        free(Gr.vertex);

        return(retval);
}


/*
 * seek_and_join: recursive function for searching for a
 * hamiltonian circuit.
 * 
 * Input:
 * graph:   a graph (as graph_t)
 * *path:   a list of u_int specifying a connected path in graph
 * *start:  a vertex, one of the endpoints of *path
 * *end:    a vertex, the other endpoint
 * *used_v: a list of used vertices of graph inside *path 
 *
 * Output:
 * the length of *path. If it is == graph.vert_n, this means
 * that *path is a hamiltonian circuit. Otherwise, it is not.
 *
 * All the parameters are modified inside the function,
 * they are all state-variables of the process.
 *
 */

u_int seek_and_join(graph_t graph, 
                    u_int *path,
                    u_int *start,
                    u_int *end,
                    u_int *length,
                    u_int *used_vertices){
        u_int i;
        for(i=0; i < graph.edge_n; i++){
                u_int A = graph.edge[i].a;
                u_int B = graph.edge[i].b;

                /*
                 * Last case: length == n-1 is special
                 */
                if(*length == graph.vert_n-1){
                        if((A == *start && B == *end  ) ||
                           (A == *end   && B == *start)){
                                path[(*length)++] = i;
                                return(*length);
                        }
                        continue;
                }

               /* 
                * Can this arc be joined at any of the *endpoints?
                */
                if(A == *start && !used_vertices[B]){
                        *start = B;
                } else if(A == *end && !used_vertices[B]){
                        *end = B;
                } else if(B == *start && !used_vertices[A]){
                        *start = A;
                } else if(B == *start && !used_vertices[A]){
                        *end = A;
                } else 
                        continue;

                used_vertices[A]  = 1;
                used_vertices[B]  = 1;
                path[(*length)++] = i;

                if(seek_and_join(graph, 
                                 path,
                                 start,
                                 end,
                                 length,
                                 used_vertices) == graph.vert_n)
                        return(graph.vert_n);

                /* 
                 * The recursion has not given a solution: 
                 * remove last added edge from path and continue.
                 */
                (*length)--;

                if(*start == A){
                        *start = B;
                        used_vertices[A] = 0;
                } else if(*end == A) {
                        *end = B;
                        used_vertices[A] = 0;
                } else if(*start == B) {
                        *start = A;
                        used_vertices[B] = 0;
                } else {
                        *end = A;
                        used_vertices[B] = 0;
                }
        }

        /* 
         * No edge has been useful for building a solution:
         * return the present length: at this point
         * it is always != graph.vert_n.
         */
        return (*length);
}
