/* seek_ham.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <err.h>
#include <unistd.h>

#include "hamiltonian.h"

/* 
 * seek_ham
 * Given a graph (see make_graph.c) which supposedly contains
 * a Hamiltonian Path, search for one and report it.
 *
 * Input:
 * s: a string describing a graph, as per make_graph.c
 * t: (optional) maximum number of seconds to perform seek
 *
 * Output:
 * 
 * a string of comma-separated numbers which correspond to the
 * edges in the graph building up a hamiltonian path.
 *
 * On stderr, both the received graph and the elapsed time
 * are reported.
 */


int main(int argc, char *argv[]){
        int retval = 0;

        /* verify that input_txt is not NULL */
        if(argc < 2 || argv[1] == NULL){
                fprintf(stderr, ERR_BAD_STRING_S);
                exit(ERR_BAD_STRING);
        }
        
        if((retval = process_graph(argv[1])) != 0){
                exit(retval);
        };

        exit(retval);
}


int process_graph(char *input_txt){
        int retval = 0;
        
        graph_t Gr;
        Gr.vertex = NULL;
        Gr.edge   = NULL;

        /* parameters for solution seeking */
        u_int *path          = NULL;
        u_int *used_vertices = NULL;
        u_int path_length    = 0;
        u_int start, end, i;        

        if((retval = parse_graph(&Gr, input_txt)) != 0)
                goto FREE;

        /*
         * Do seek the hamiltonian path.
         */
        path =          (u_int *)calloc(Gr.vert_n, sizeof(u_int));
        used_vertices = (u_int *)calloc(Gr.vert_n, sizeof(u_int));
        if(path == NULL || used_vertices == NULL){
                fprintf(stderr, "Unable to allocate memory\n");
                retval = ERR_OS;
                goto FREE;
        }

        for(i=0; i<Gr.edge_n; i++){
                u_int A = Gr.edge[i].a;
                u_int B = Gr.edge[i].b;
                start = A;
                end   = B;
                path[path_length++] = i;
                used_vertices[A] = 1;
                used_vertices[B] = 1;
        
                path_length = seek_and_join(Gr,
                                            path,
                                            start,
                                            end,
                                            path_length,
                                            used_vertices);
                if(path_length == Gr.vert_n)
                        break;

                used_vertices[A] = 0;
                used_vertices[B] = 0;
        }


        /*
         * Failure: report and exit.
         */
        if(path_length != Gr.vert_n){
                fprintf(stderr, FAIL_NO_HAM_S);
                retval = FAIL_NO_HAM;
                goto FREE;
        }

        /* 
         * Success: report on stdout.
         */
        for(i=0; i<Gr.vert_n-1; i++)
                printf("%u,", path[i]);
        printf("%u\n", path[i]);

FREE:
        free(path);
        free(used_vertices);
        free(Gr.edge);
        free(Gr.vertex);

        return(retval);
}


u_int seek_and_join(graph_t graph, 
                    u_int *path,
                    u_int start,
                    u_int end,
                    u_int length,
                    u_int *used_vertices){
        u_int head = 0;

        /* can be inserted in the for */
        while(length > 0){
                u_int i;
        ARC_LOOP:
                for(i=head; i< graph.edge_n && length > 0; i++){
                        u_int A = graph.edge[i].a;
                        u_int B = graph.edge[i].b;

                        if(length == graph.vert_n - 1){
                                if((A == start && B == end  ) ||
                                   (A == end   && B == start)){
                                        path[(length)++] = i;
                                        return(length);
                                }
                                continue;
                        }

                        /* 
                         * Can this arc be joined at one endpoint?
                         */
                        if(A == start && !used_vertices[B]){
                                start = B;
                        } else if(A == end && !used_vertices[B]){
                                end = B;
                        } else if(B == start && !used_vertices[A]){
                                start = A;
                        } else if(B == start && !used_vertices[A]){
                                end = A;
                        } else 
                                continue;
                        
                        used_vertices[A]  = 1;
                        used_vertices[B]  = 1;
                        path[(length)++] = i;
                        /* if success, add path and
                         * goto ARC_LOOP starting at the first arc
                         */
                        
                        head = 0;
                        goto ARC_LOOP;
                }

                /* all arcs unsuccessful, this means the
                 * last one in path is useless,
                 * remove and try the head one if
                 * there is one:
                 * POP(last_arc)
                 * 
                 */
                length--;
                u_int A = graph.edge[path[length]].a;
                u_int B = graph.edge[path[length]].b;
                
                if(start == A){
                        start = B;
                        used_vertices[A] = 0;
                } else if(end == A) {
                        end = B;
                        used_vertices[A] = 0;
                } else if(start == B) {
                        start = A;
                        used_vertices[B] = 0;
                } else {
                        end = A;
                        used_vertices[B] = 0;
                }
                head = path[length] + 1;
        }
        return(length);
}
