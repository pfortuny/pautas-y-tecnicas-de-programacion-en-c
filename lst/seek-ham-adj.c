/* seek_ham.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <err.h>
#include <unistd.h>

#include "hamiltonian.h"


/* 
 * seek_ham
 * Given a graph (see make_graph.c) which supposedly contains
 * a Hamiltonian Path, search for one and report it.
 *
 * Input:
 * s: a string describing a graph, as per make_graph.c
 * t: (optional) maximum number of seconds to perform seek
 *
 * Output:
 * 
 * a string of comma-separated numbers which correspond to the
 * edges in the graph building up a hamiltonian path.
 *
 * On stderr, both the received graph and the elapsed time
 * are reported.
 */


int main(int argc, char *argv[]){
        int retval = 0;

        /* verify that input_txt is not NULL */
        if(argc < 2 || argv[1] == NULL){
                fprintf(stderr, ERR_BAD_STRING_S);
                exit(ERR_BAD_STRING);
        }
        
        if((retval = process_graph(argv[1])) != 0){
                exit(retval);
        };

        exit(retval);
}


/* process_graph
 *
 * Input:
 * char *input_txt: a string containing the graph as per
 *                  make_graph
 */
int process_graph(char *input_txt){
        int retval;

        u_int i;
        graph_t graph;
        graph.vertex = NULL;
        graph.edge   = NULL;

        adjacent_t **adjacency_m = NULL;

        u_int *e_path = NULL;
        u_int *v_path = NULL;
        u_int *used_v = NULL;

        if((retval = parse_graph(&graph, input_txt)) != 0)
                return(retval);

        /*
         * Adjacency matrix: actually a list of adjacency lists.
         */
        adjacency_m = (adjacent_t **) calloc(graph.vert_n, 
                                             sizeof(adjacent_t *));

        if(adjacency_m == NULL){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto FREE;
        }

        for(i=0; i<graph.edge_n; i++){
                if((retval = push_adjacent(adjacency_m,
                                           graph.edge[i].a,
                                           graph.edge[i].b,
                                           i)) != 0){
                        goto FREE; //memory error
                }
                        
                if((retval = push_adjacent(adjacency_m,
                                           graph.edge[i].b,
                                           graph.edge[i].a,
                                           i)) != 0){
                        goto FREE; //memory error
                }
        }


        /* 
         * Check that ALL vertices have adjacents,
         * otherwise there is no hamiltonian path.
         */
        for(i=0; i<graph.vert_n; i++){
                if(adjacency_m[i] == NULL){
                        fprintf(stderr, ERR_ADJACENCY_0_S);
                        retval = ERR_ADJACENCY_0;
                        goto FREE;
                }
        }
        
        e_path = (u_int *) calloc(graph.vert_n, sizeof(u_int));
        v_path = (u_int *) calloc(graph.vert_n, sizeof(u_int));
        used_v = (u_int *) calloc(graph.vert_n, sizeof(u_int));
        
        if(e_path == NULL || 
           v_path == NULL ||
           used_v == NULL){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto FREE;
        }

        retval = iterate_vertices(adjacency_m,
                                  graph.vert_n,                 
                                  e_path,
                                  v_path,
                                  used_v);
        if(retval != 0)
                goto FREE;

        for(i=0; i<graph.vert_n-1; i++)
                printf("%u,", e_path[i]);
        printf("%u\n", e_path[i]);

FREE:
        /*
         * Free!
         */
        if(adjacency_m != NULL){
                for(i=0; i<graph.vert_n; i++){
                        adjacent_t *pt, *next;
                        pt = adjacency_m[i];
                        while(pt != NULL){
                                next = pt->next;
                                free(pt);
                                pt = next;
                        }
                }
        }
        free(adjacency_m);
        free(graph.edge);
        free(graph.vertex);
        free(e_path);
        free(v_path);
        free(used_v);
        return(retval);

}


int iterate_vertices(adjacent_t **adjacency_m,
                     u_int vertex_nbr,
                     u_int *e_path,
                     u_int *v_path,
                     u_int *used_v){

        /* Failure by default. If success, value is set to 0 */
        int retval =  FAIL_NO_HAM;

        /* 
         * Start at vertex 0: as a matter of fact, we should
         * look for the vertex with least adjacents and start
         * there. But it looks like the gain for doing this will be
         * slight.
         */
        u_int l = 0;

        /*
         * List of vertices adjacent to the "present" one.
         * This is for scanning pointer for adjacency_m[i].
         */
        adjacent_t *adjacent_p = NULL;

        /*
         * Vertex list (of the path) with adjacency info.
         * This is needed for backtracking.
         */
        adjacent_t **bt_path = NULL;

        v_path[l] = 0;
        used_v[v_path[l]] = 1;

        adjacent_p = adjacency_m[v_path[0]];

        bt_path = (adjacent_t **) calloc(vertex_nbr, 
                                         sizeof(adjacent_t*));
        if(bt_path == NULL){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto RET;
        }

        bt_path[l] = adjacent_p;

        /*
         * Infinite loop: it is broken if l *would become* -1
         * (which implies there is no hamiltonian path).
         */
SCAN_ADJ:
        while(adjacent_p){
                u_int next_v = adjacent_p->vertex;
                if(l == vertex_nbr - 1 &&
                   next_v == v_path[0]){
                        e_path[l] = adjacent_p->edge_n;
                        retval = 0;
                        goto RET;
                }
                
                if(!used_v[next_v]){
                        e_path[l++] = adjacent_p->edge_n;
                        v_path[l] = next_v;
                        bt_path[l] = adjacent_p;
                        used_v[next_v] = 1;
                        adjacent_p = adjacency_m[next_v];
                        goto SCAN_ADJ;
                }

                adjacent_p = adjacent_p->next;
        }
        
        if(l != 0){
                /* backtracking */
                used_v[v_path[l]] = 0;
                adjacent_p = bt_path[l]->next;
                l--;
                goto SCAN_ADJ;
        }

RET:
        if(retval == FAIL_NO_HAM)
                fprintf(stderr, FAIL_NO_HAM_S);
        
        free(bt_path);
        
        return(retval);
}
