/* seek_ham.c (c) Pedro Fortuny Ayuso, 2012
 * See LICENCE for usage (essentially unlimited).
 */
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <err.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/select.h>

#include <signal.h>

#include "hamiltonian.h"


/* 
 * seek_ham
 * Given a graph (see make_graph.c) which supposedly contains
 * a Hamiltonian Path, search for one and report it.
 *
 * Input:
 * s: a string describing a graph, as per make_graph.c
 * t: (optional) maximum number of seconds to perform seek
 *
 * Output:
 * 
 * a string of comma-separated numbers which correspond to the
 * edges in the graph building up a hamiltonian path.
 *
 * On stderr, both the received graph and the elapsed time
 * are reported.
 */


int main(int argc, char *argv[]){

        int retval;
        
        /* verify that input_txt is not NULL */
        if(argc < 2 || argv[1] == NULL){
                fprintf(stderr, ERR_BAD_STRING_S);
                exit(ERR_BAD_STRING);
        }

        if((retval = process_graph(argv[1])) != 0){
                exit(retval);
        };

        exit(0);
}


/* process_graph
 *
 * Input:
 * char *input_txt: a string containing the graph as per
 *                  make_graph
 */
int process_graph(char *input_txt){
  
        int retval = 0; 

        u_int i, j;

        graph_t Gr;
        Gr.vertex = NULL;
        Gr.edge = NULL;

        /* Adjacency matrix. */
        adjacent_t **adjacency_m = NULL;

        /*
         * Threads stuff.
         */
        int pipe_fds[2];
        pthread_t thread[2];
        pthread_attr_t thread_attr[2];
        seek_data_t seek_data[2];
        pthread_mutex_t pipe_m;
        pthread_mutexattr_t pipe_m_attr;

        /*
         * Select() stuff.
         */
        fd_set set;
        struct timeval timeout;
        int ret;
        char buf;
        u_int brc = 0;
        u_int *path = NULL;

        if((retval = parse_graph(&Gr, input_txt)) != 0)
                return(retval);

        /*
         * Adjacency matrix: actually a list of adjacency lists.
         */
        adjacency_m = 
                (adjacent_t **)calloc(Gr.vert_n, 
                                      sizeof(adjacent_t *));

        /*
         * Twice: one for [a,b], another one for [b,a] because
         * our graphs are not directed.
         */
        for(i=0; i<Gr.edge_n; i++){
                push_adjacent(adjacency_m,
                              Gr.edge[i].a,
                              Gr.edge[i].b,
                              i);
                push_adjacent(adjacency_m,
                              Gr.edge[i].b,
                              Gr.edge[i].a,
                              i);
        }

        /* 
         * Verify that ALL vertices have adjacents,
         * otherwise there is no hamiltonian path.
         */
        for(i=0; i<Gr.vert_n; i++){
                if(adjacency_m[i] == NULL){
                        fprintf(stderr, ERR_ADJACENCY_0_S);
                        retval = ERR_BAD_STRING;
                        goto CLEANUP;
                }
        }

        /*
         * In order to implement IPC, we are using a pipe and
         * select(). This is preferable to a file because it
         * does not taint the environment and to a socket because
         * of its simplicity in comparison.
         */

        if(pipe(pipe_fds) == -1){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto CLEANUP;
        }

        /* 
         * Thread environment: we are creating 2 threads,
         * each needs its attributes set (to default values)
         * and there is a mutex for the write-end of the pipe.
         */
        for(i=0; i<2;i++){
                if(pthread_attr_init(&thread_attr[i]) != 0){
                        fprintf(stderr, ERR_OS_S);
                        retval = ERR_OS;
                        goto CLEANUP;
                }                     
        }
        
        /*
         * Thread data.
         * In order to pass data to each one, we use
         * the seek_data struct and put all that is needed into it.
         * Obviously, each thread gets its own data (most of it will
         * be common, but some will not).
         */
        if(pthread_mutexattr_init(&pipe_m_attr) !=0){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto CLEANUP;
        }
        
        if(pthread_mutexattr_settype(&pipe_m_attr, 
                                     PTHREAD_MUTEX_ERRORCHECK) != 0){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto CLEANUP;        
        }
        
        if(pthread_mutex_init(&pipe_m, &pipe_m_attr) !=0){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto CLEANUP;
        }

        for(j=0; j<2; j++){
                
                seek_data[j].graph = &Gr;
                seek_data[j].adjacency_m = adjacency_m;
                seek_data[j].start_v = j;
                seek_data[j].w_pipe = pipe_fds[1];
                seek_data[j].pipe_m = &pipe_m;
                
                if(pthread_create(&thread[j],
                                   &thread_attr[j],
                                   &(seek_thread),
                                   (void *)&seek_data[j]) != 0){
                        fprintf(stderr, ERR_OS_S);
                        retval = ERR_OS;
                        goto CLEANUP;
                }
        }

        /*
         * Select() stuff:
         * we need a set of read file descriptors, in this case
         * composed only of the read-end of the pipe,
         * and a timeout
         */
        FD_ZERO(&set);
        FD_SET(pipe_fds[0], &set);

        timeout.tv_sec = 1;
        timeout.tv_usec = 0;

        path = (u_int *) calloc(Gr.vert_n, sizeof(u_int));
        if(path == NULL){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto CLEANUP;
        }

        /* select */
        while((ret = 
               select(pipe_fds[0] + 1, &set, 
                      NULL, NULL, &timeout)) != -1){
                
                /* reset select() variables first of all */
                FD_SET(pipe_fds[0], &set);
                timeout.tv_sec = 1;
                timeout.tv_usec = 0;

                if(ret == 0)
                        continue;

                /* there is data to be read, positively */
                int br = 0;
                if(brc < 1){ // first byte to be read: '0' or '1'
                        br = read(pipe_fds[0], &buf, 1);
                        if(br <= 0){
                                fprintf(stderr, ERR_OS_S);
                                retval = br;
                                goto CLEANUP;
                        }

                        brc += br;
 
                        /* if a thread sends a '0', there is NO path */
                        if(buf == '0'){
                                fprintf(stderr, FAIL_NO_HAM_S);
                                retval = FAIL_NO_HAM;
                                goto CLEANUP;
                        }
                }
                
                /* 
                 * At this point, IF there is data in the pipe to be
                 * read, it is positively a path (or part of one). 
                 * The length of a path is exactly 
                 *     Gr.vert_n * sizeof(u_int).
                 */

                br = read(pipe_fds[0], 
                          &((char *)path)[brc-1], 
                          Gr.vert_n*sizeof(u_int)-(brc-1));

                if(br <= -1){
                        fprintf(stderr, ERR_OS_S);
                        retval = br;
                        goto CLEANUP;
                }

                if(br == 0)
                        continue;

                brc += br;
                        
                /* 
                 * When brc == Gr.vert_n*4 bytes, 
                 * the path has been completely read. 
                 */
                if(brc >= 1 + Gr.vert_n*sizeof(u_int))
                        goto SHOW_SOL;
        }

        /* 
         * One only gets here if the path has been successfully read().
         */
SHOW_SOL:
        for(i=0; i<Gr.vert_n; i++){
                printf("%u", path[i]);
                if(i<Gr.vert_n-1)
                        printf(",");
                
        }
        printf("\n");

        /* 
         * Cleanup the threads, we do not care about exit status here
         * because we are terminating. All variables have already been
         * initialized (pointers as NULL at least).
         */
CLEANUP:
        for(i=0;i<2;i++)
                //pthread_kill(thread[i],9);
                pthread_cancel(thread[i]);

        /* We still want to join because cancelling is asynchronous */
        for(i=0;i<2;i++)
                pthread_join(thread[i], NULL);

        /* 
         * Do not care about exit status because we are just
         * cleaning up.
         */
        close(pipe_fds[0]);
        close(pipe_fds[1]);

        /*
         * Free!
         */
        if(adjacency_m){
                for(i=0; i<Gr.vert_n; i++){
                        adjacent_t *pt, *next;
                        pt = adjacency_m[i];
                        while(pt!=NULL){
                                next = pt->next;
                                if(pt) free(pt);
                                pt = next;
                        }
                }
        }

        free(path);
        free(adjacency_m);
        free(Gr.edge);
        free(Gr.vertex);

        return(retval);
}

int iterate_vertices(adjacent_t **adjacency_m,
                     u_int vertex_nbr,
                     u_int *v_path,
                     u_int *e_path,
                     u_int *used_v,
		     adjacent_t **bt_path){

        u_int l = 0;
        /* used_v, v_path and e_path are already started */
        /* this differs from -adj.c */

        /*
         * List of vertices adjacent to the "present" one.
         * This is for scanning pointer for adjacency_m[i].
         */
        adjacent_t *adjacent_p = adjacency_m[v_path[0]];

        bt_path[l] = adjacent_p;

        /*
         * Infinite loop: it is broken if l *would become* -1
         * (which implies there is no hamiltonian path).
         */
SCAN_ADJ:
        while(adjacent_p){
                pthread_testcancel();
                u_int next_v = adjacent_p->vertex;
                if(l == vertex_nbr - 1 &&
                   next_v == v_path[0]){
                        e_path[l] = adjacent_p->edge_n;
                        return(0);
                }
                
                if(!used_v[next_v]){
                        e_path[l++] = adjacent_p->edge_n;
                        v_path[l] = next_v;
                        bt_path[l] = adjacent_p;
                        used_v[next_v] = 1;
                        adjacent_p = adjacency_m[next_v];
                        goto SCAN_ADJ;
                }

                adjacent_p = adjacent_p->next;
        }
        
        if(l != 0){
                /* backtrack */
                used_v[v_path[l]] = 0;
                adjacent_p = bt_path[l]->next;
                l--;
                goto SCAN_ADJ;
        }
        
        free(bt_path);
        
        /* 
         * If this point is reached, there is no hamiltonian path.
         */
        return(FAIL_NO_HAM);
        
}


void *seek_thread(void *data){
        seek_data_t *input = 
                (seek_data_t *)(data);
        graph_t gr    = *(input->graph);
        u_int start_v = input->start_v;
        int w_pipe    = input->w_pipe;
        pthread_mutex_t *pipe_m = 
                input->pipe_m;
        adjacent_t **adjacency_m = 
                input->adjacency_m;

        u_int retval = 0;

        u_int *e_path = NULL;
        u_int *v_path =NULL;
        u_int *used_v = NULL;
        adjacent_t **bt_path = NULL;

        e_path     = (u_int *)calloc(gr.vert_n, sizeof(u_int ));
        v_path   = (u_int *)calloc(gr.vert_n, sizeof(u_int ));
        used_v = (u_int *) calloc(gr.vert_n, sizeof(u_int));

        /*
         * Vertex list (of the path) with adjacency info.
         * This is needed for backtracking.
         */
        bt_path = (adjacent_t **)
	        calloc(gr.vert_n, sizeof(adjacent_t*));

        if(e_path == NULL || v_path == NULL || 
	   used_v == NULL || bt_path == NULL)
                pthread_exit(NULL);
        
        e_path[0] = start_v;
        v_path[0] = start_v;
        used_v[start_v] = 1;
        
        /*
         * Cleanup setup (most threads are going to be
         * canceled, cleaning up is imperative).
         */
        cleanup_data_t cleanup_d;
        cleanup_d.pipe_m  = pipe_m;
        cleanup_d.e_path  = e_path;
        cleanup_d.v_path  = v_path;
        cleanup_d.used_v  = used_v;
	cleanup_d.bt_path = bt_path;

        pthread_cleanup_push((&(cleanup_threads)), 
                             ((void *) &cleanup_d));

       
        /* 
         * Finally, do the real job:
         * DIFF: we return the length!
         */
        retval = iterate_vertices(adjacency_m, 
				  gr.vert_n, 
				  v_path, 
				  e_path, 
				  used_v,
				  bt_path);

        /* Lock w_pipe */
        if(pthread_mutex_lock(pipe_m) != 0)
                pthread_exit(NULL);

        /* Success: send a '1' and then the solution */
        if(retval == 0){
                /* send the '1' */
                if(write(w_pipe, "1", 1) != 1){
                        pthread_mutex_unlock(pipe_m);
                        pthread_exit(NULL);
                }
                /* send the path as a sequence of u_int */
                u_int i;
                for(i=0; i<gr.vert_n; i++){
                        if(write(w_pipe, 
                                 &(e_path[i]), 
                                 sizeof(u_int)) !=sizeof(u_int)){
                                pthread_mutex_unlock(pipe_m);
                                pthread_exit(NULL);
                        }
                }
        } else if(retval == FAIL_NO_HAM) { /* No so: send '0' & end */
                write(w_pipe, "0", 1);
        } else { /* OS_ERR, already logged, exit silently (by now) */
                pthread_exit(NULL);
        }

        /* No exit value check: we are already done */
        close(w_pipe);
        pthread_mutex_unlock(pipe_m);
        
        pthread_cleanup_pop(1);
        pthread_exit(NULL);
}

void cleanup_threads(void *data){        
        cleanup_data_t *cleanup_d = data;
        /* No error control: we are finishing */
        pthread_mutex_unlock(cleanup_d->pipe_m);
        free(cleanup_d->e_path);
        free(cleanup_d->v_path);
        free(cleanup_d->used_v);
	free(cleanup_d->bt_path);
}


