/* test_ham.c (c) Pedro Fortuny Ayuso, 2012
 * This file is in the Public Domain.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

#include "hamiltonian.h"

/* test-ham
 * 
 * Check whether a sequence of edges in a graph is a Hamiltonian
 * path.
 *
 * Input:
 * g: a graph as output by make_graph (length at most 1024 chars):
 * p: a sequence of comma-separated numbers indicating a path (each
 *       number represents the corresponding edge in the graph).
 *
 * Output:
 * a test string:
 * either FAIL!! (the sequence of edges does not correspond to a
 *       hamiltonian path): exit status -1.
 * or the exact path as a sequence of vertices (unordered):
 *       exit status 0.
 * 
 */

int main (int argc, char *argv[]){
        int retval = 0;

        char *the_graph = NULL;
        char *edge_seq  = NULL; 

        graph_t graph;

        char *vertices = NULL;

        u_int path_length = 0;
        char *edge_as_char = NULL;

        int FIRST = 1;

        if(argc < 3){
                printf(USAGE_TEST_HAM, argv[0]);
                retval = ERR_USG;
                goto FREE;
        }

        the_graph  = argv[1];
        edge_seq   = argv[2];

        if((retval = parse_graph(&graph, the_graph)) != 0)
                return(retval);

        /*
         * vertices[i] measures how many times the path passes through
         * vertex i.
         */
        vertices = (char *)calloc(graph.vert_n, sizeof(char));
        if(vertices == NULL){
                fprintf(stderr, ERR_OS_S);
                retval = ERR_OS;
                goto FREE;
        }

        while((edge_as_char = strsep(&edge_seq, ",")) != NULL){

                u_int e_n, a, b, s, e;
                e_n = (u_int )strtol(edge_as_char, NULL, 10);
        
                if(e_n > graph.edge_n){
                        fprintf(stderr, FAIL_WRONG_EDGE_S);
                        retval = FAIL_WRONG_EDGE;
                        goto FREE;
                }

                a = graph.edge[e_n].a;
                b = graph.edge[e_n].b;

                if(FIRST){
                        s = a;
                        e = b;
                        FIRST = 0;
                        goto INC;
                }

                if(a == s){
                        s = b;
                } else if(a == e) {
                        e = b;
                } else if(b == s) {
                        s = a;
                } else if(b == e){
                        e = a;
                } else{
                        fprintf(stderr, FAIL_NO_PATH_S);
                        retval = FAIL_NO_PATH;
                        goto FREE;
                }

                if(vertices[a] >= 2 ||
                   vertices[b] >= 2){
                        fprintf(stderr, FAIL_REPEATED_VTX_S);
                        retval = FAIL_REPEATED_VTX;
                        goto FREE;
                }

        INC:
                vertices[a]++;
                vertices[b]++;
                path_length++;
                if(path_length > graph.vert_n){
                        fprintf(stderr, FAIL_TOO_MANY_EDGES_S);
                        retval = FAIL_TOO_MANY_EDGES;
                        goto FREE;
                }
        }

       /* at this point either
        * path_length < graph.vert_n => NOT hamiltonian
        * or
        * the path is truly hamiltonian.
        */
        if(path_length < graph.vert_n){
                fprintf(stderr, FAIL_TOO_SHORT_S);
                retval = FAIL_TOO_SHORT;
                goto FREE;
        }

        printf("The path is hamiltonian.\n");

FREE:
        free(graph.edge);
        free(graph.vertex);
        free(vertices);

        return(retval);
}
